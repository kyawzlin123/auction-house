GlobalMarket
============

A Bukkit plugin that allows players to buy and sell items in a global market with a sweet GUI!

## Version 2.0.2.1

Updated to 1.13.2
Minor changes

### To-do
* Transaction history
* Pricing history
* Multi-world support
* ~~Infinite listings~~
* Stalls (will be named Terminals)
* Permissions-based restrictions for stock, listings
* Listings filtering and searching
* Buy requests
